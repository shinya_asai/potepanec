require 'rails_helper'
RSpec.describe ApplicationHelper, type: :helper do
  let(:product) { create(:product) }

  describe "full_titleヘルパー" do
    context "page_titleが空の場合" do
      it "base_titleを表示すること" do
        expect(full_title('')).to eq "potepanEC"
      end
    end

    context "page_titleが空ではない場合" do
      it "page_title(product.name) - bace_titleをを表示すること" do
        expect(full_title(product.name)).to eq "#{product.name} - potepanEC"
      end
    end
  end
end
