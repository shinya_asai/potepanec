require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /potepan/categories" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "正常にレスポンスが返ってくること" do
      expect(response).to have_http_status(200)
    end

    it "@taxonomyが取得出来ていること" do
      expect(response.body).to include taxonomy.name
    end

    it "@taxonが取得出来ていること" do
      expect(response.body).to include taxon.name
    end

    it "@productが取得出来ていること" do
      expect(response.body).to include product.name
    end
  end
end
