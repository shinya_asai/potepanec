require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, name: 'Bag', taxons: [taxon]) }
  let(:other_product) { create(:product, name: 'Mag', taxons: [other_taxon]) }
  let!(:related_products) { create_list(:product, 5, name: "T", taxons: [taxon]) }

  describe "GET /potepan/products/id" do
    before { get potepan_product_path(product.id) }

    it "正常にレスポンスが返ってくること" do
      expect(response).to have_http_status(200)
    end

    it "showテンプレートが表示されること" do
      expect(response).to render_template :show
    end

    it "商品名が表示されること" do
      expect(response.body).to include product.name
    end

    it "商品の値段が表示されること" do
      expect(response.body).to include product.display_price.to_s
    end

    it "商品の説明が表示されること" do
      expect(response.body).to include product.description
    end

    context "関連商品がある場合" do
      it "関連商品に商品自身は表示されないこと" do
        within ".productBox" do
          expect(page).not_to have_content product.name
        end
      end

      it "関連していない商品は表示されないこと" do
        expect(response.body).not_to include other_product.name
      end

      it "関連商品が最大４つまで表示されること" do
        expect(Capybara.string(response.body)).to have_selector ".productBox", count: 4
        expect(Capybara.string(response.body)).not_to have_selector ".productBox", count: 5
      end
    end
  end
end
