require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  it"関連商品のリンク先が商品の詳細ページになっていること" do
    click_link "#{related_product.name}"
    expect(current_path).to eq potepan_product_path(related_product.id)
    expect(page).to have_content related_product.name
    expect(page).to have_content related_product.display_price
  end

  it "一覧へ戻るをクリックするとその商品のカテゴリーページを表示すること" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end
end
