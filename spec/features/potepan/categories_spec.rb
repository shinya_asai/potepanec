require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, name: "Bags", taxonomy: taxonomy) }
  let!(:other_taxon) { create(:taxon, name: "Mugs", taxonomy: taxonomy) }
  let!(:product) { create(:product, name: "RUBY ON RAILS TOTE", taxons: [taxon]) }
  let!(:other_product) { create(:product, name: "RUBY ON RAILS MUG", taxons: [other_taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  scenario "ページタイトルが表示される" do
    expect(page).to have_content taxon.name
  end

  scenario "サイドバーに商品カテゴリーが表示される" do
    within(".side-nav") do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content "#{taxon.name} (#{taxon.products.count})"
    end
  end

  scenario "カテゴリー別に商品名、商品価格が表示される" do
    click_link "#{taxon.name} (#{taxon.products.count})"
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  context "カテゴリーが異なる場合" do
    scenario "対象の商品は表示されるが対象外の商品は表示されない" do
      click_link "#{other_taxon.name} (#{other_taxon.products.count})"
      expect(page).to have_content other_product.name
      expect(page).not_to have_content product.name
    end
  end

  scenario "表示されている商品のリンク先が商品詳細ページになっている" do
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
